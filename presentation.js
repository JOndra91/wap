/**
 * @author Ondřej Janošík <utils12@stud.fit.vutbr.cz>
 */

/****************************** Utility **************************************/

if(typeof Symbol == 'undefined') {
  Symbol = {};
}

if(typeof Symbol.iterator == 'undefined') {
  Symbol.iterator = '@@iterator';
}

String.prototype.upperCaseFirst = function() {
  return this[0].toUpperCase() + this.slice(1);
}

String.prototype.format = function() {
  var args = arguments;
  return this.replace(/(\\?){(\d)}/g, function(match, escape, number) {
    return (escape.length == 0 && typeof args[number] != 'undefined')
      ? args[number] : match;
  });
};

if(typeof Array.from == 'undefined') {
  console.log('Creating Array.from');
  Array.from = function(iterable, fn, thisArg) {
    var array = [];
    if(typeof iterable[Symbol.iterator] == 'function') {
      var iterator = iterable[Symbol.iterator]();

      for (;;) {
        var next = iterator.next();
        if(next.done) {
          break;
        }
        array.push(next.value);
      }
    }
    else if(iterable instanceof Set) {
      iterable.forEach(function(value) {
        array.push(value);
      });
    }
    else if(typeof iterable.length == 'number') {
      for(var i = 0; i < iterable.length; ++i) {
        array.push(iterable[i]);
      }
    }
    else {
      console.log('Could not convert to array.');
    }

    if(typeof fn == 'function') {
      return array.map(fn, thisArg);
    }
    else {
      return array;
    }
  };
}

if(typeof Array.prototype.includes == 'undefined') {
  Array.prototype.includes = function(value) {
    return this.indexOf(value) != -1;
  }
}

if(typeof Array.prototype.find == 'undefined') {
  Array.prototype.find = function(callback, thisArg) {
    var found = false;
    var value = this.reduce(function(acc, current, index, array){
      if(found) {
        return acc;
      }

      if(callback.call(thisArg, current, index, array)) {
        found = true;
        return current;
      }
    }, null);

    if(found) {
      return value;
    }
  }
}

{
  var utils = function(context, selector){
    return utils.constr(context, selector);
  };

  utils.init = function() {

    this.Collection = function(elements) {

      this.elements = new Array();

      // Doesn't work with IE!
      // this.find = function(selector) {
      //   var found = new Set();
      //   this.elements.forEach(function(contextNode){
      //     var result = document.evaluate(selector, contextNode, null, XPathResult.ANY_TYPE, null);
      //     var node;
      //     while((node = result.iterateNext()) != null) {
      //       found.add(node);
      //     }
      //   });
      //
      //   return new utils.Collection(found);
      // };

      this.add = function(elements) {
        if(elements instanceof utils.Collection) {
          elements.forEach(function(el) {
            this.add(el);
          }, this);
        }
        else if(utils.isIterable(elements)) {
          Array.from(elements).forEach(function(el) {
            this.add(el);
          }, this);
        }
        else if(elements instanceof Node) {
          if(!this.elements.includes(elements)) {
            this.elements.push(elements);
          }
        }
        else {
          utils.error('Parameter of utils.Collection.add is not utils.Collection, Node or array of Nodes.');
        }
      }

      this.findClass = function(className) {
        var found = new Set();
        this.elements.forEach(function(node) {
          Array.from(node.getElementsByClassName(className))
            .forEach(function(n) {
              found.add(n);
            });
        });
        return new utils.Collection(found);
      };

      this[Symbol.iterator] = function() {
        return this.elements.keys;
      }

      this.forEach = function(callback, thisArg) {
        this.elements.forEach(callback, thisArg);
        return this;
      };

      this.firstElement = function() {
        return this.elements[0];
      }

      this.parent = function() {
        return utils(this.firstElement().parentNode);
      }

      this.parents = function() {
        var parents = [];
        var node = this.firstElement();

        while(node.parentNode) {
          node = node.parentNode;
          parents.push(node);
        }

        return utils(parents);
      }

      this.addClass = function(className) {
        this.forEach(function(el){
          if (className instanceof Array) {
            className.forEach(function(className) {
              el.classList.add(className);
            });
          }
          else {
            el.classList.add(className);
          }
        });
        return this;
      };

      this.toggleClass = function(className) {
        this.forEach(function(el){
          if (className instanceof Array) {
            className.forEach(function(className) {
              el.classList.toggle(className);
            });
          }
          else {
            el.classList.toggle(className);
          }
        });
        return this;
      };

      this.removeClass = function(className) {
        this.forEach(function(el){
          if (className instanceof Array) {
            className.forEach(function(className) {
              el.classList.remove(className);
            });
          }
          else {
            el.classList.remove(className);
          }
        });
        return this;
      };

      this.hasClass = function(className) {
        return this.firstElement()
          && (typeof this.firstElement().classList != 'undefined')
          && this.firstElement().classList.contains(className);
      };

      this.css = function(css) {
        this.forEach(function(el){
          for(var prop in css) {
            el.style[prop] = css[prop];
          }
        });
        return this;
      };

      this.prop = function(props) {
        this.forEach(function(el){
          for(var prop in props) {
            el[prop] = props[prop];
          }
        });
        return this;
      };

      this.on = function(type, listener, useCapture) {
        this.forEach(function(el) {
          if(type instanceof Array) {
            type.forEach(function(type) {
              el.addEventListener(type, listener, useCapture);
            });
          }
          else {
            el.addEventListener(type, listener, useCapture);
          }
        });

        return this;
      }

      this.appendChild = function(child) {
          if(child instanceof Node) {
            this.firstElement().appendChild(child);
          }
          else if(utils.isIterable(child)) {
            child.forEach(function(c) {
              this.appendChild(c);
            }, this);
          }
          return this;
      }

      this.insertBefore = function(sibling) {
        if(sibling instanceof Node) {
          var el = this.firstElement();
          sibling.parentNode.insertBefore(el, sibling);
        }
        else if(utils.isIterable(sibling)) {
          sibling.forEach(function(c) {
            this.insertBefore(c);
          }, this);
        }
        return this;
      }

      this.insertAfter = function(sibling) {
        if(sibling instanceof Node) {
          var el = this.firstElement();
          sibling.parentNode.insertBefore(el, sibling.nextElementSibling);
        }
        else if(utils.isIterable(sibling)) {
          sibling.forEach(function(c) {
            this.insertAfter(c);
          }, this);
        }
        return this;
      }

      this.empty = function() {
        this.forEach(function(parent) {
          Array.from(parent.children).forEach(function(child) {
            parent.removeChild(child);
          });
        });

        return this;
      }

      this.remove = function() {
        this.forEach(function(child) {
          if(child.parentNode) {
            child.parentNode.removeChild(child);
          }
        });

        return this;
      }

      this.replace = function(target) {
        if(target instanceof Node) {
          target.parentNode.replaceChild(this.firstElement(),target);
        }
        else if(target instanceof utils.Collection) {
          this.replace(target.firstElement());
        }

        return this;
      }

      this.clone = function(deep) {
        var clones = this.elements.map(function(node) {
          return node.cloneNode(deep);
        });

        return $(clones);
      }

      this.hide = function() {
        this.forEach(function(el) {
          if(el.style.display != 'none') {
            $._data(el,{display: el.style.display});
          }
          el.style.display = 'none';
        });

        return this;
      }

      this.show = function() {
        this.forEach(function(el) {
          el.style.display = $._data(el,'display','');
        });

        return this;
      }

      this.requestFullscreen = function() {
        var node = this.firstElement();
        var fullscreenFn
          =  utils.prefixVendor('requestFullScreen', node, 'function')
          || utils.prefixVendor('requestFullscreen', node, 'function');

        if(fullscreenFn) {
          node[fullscreenFn].call(node);
        }
      }

      if(typeof elements == 'undefined' || elements == null) {
        utils.warn('Empty collection');
        return; // Empty collection.
      }

      if(typeof elements != 'object') {
        utils.error('Parameter of Collection is not an object.');
        return;
      }

      if(elements instanceof Node) {
        this.elements.push(elements);
      }
      else if(utils.isIterable(elements)) {
        this.elements = Array.from(elements);

        if (this.elements[0] instanceof utils.Collection) {
          var l = this.elements.map(function(coll) { return coll.elements; });
          var set = new Set();

          l.forEach(function(nodes) {
            nodes.forEach(set.add.bind(set));
          });

          this.elements = Array.from(set);
        }
      }
      else {
        utils.error('Parameter of Collection is not an Set or Node.');
      }
    };

    this.LOG_LEVEL = {
      None : 0,
      Info : 1,
      Warn : 2,
      Error : 3
    };

    this.LOG_LEVEL_NAME = [
      'None',
      'Info',
      'Warn',
      'Error'
    ];

    this.logLevel = this.LOG_LEVEL.Error;

    this.log = function(logLevel, message) {
      if(this.logLevel && this.logLevel >= logLevel) {
        if(typeof message == 'object') {
          console.log("{0}:".format(this.LOG_LEVEL_NAME[logLevel]));
          console.log(message);
        }
        else {
          console.log("{0}: {1}".format(this.LOG_LEVEL_NAME[logLevel], message));
        }
      }
    };

    this.info = function(message) {this.log(this.LOG_LEVEL.Info, message)};
    this.warn = function(message) {this.log(this.LOG_LEVEL.Warn, message)};
    this.error = function(message) {this.log(this.LOG_LEVEL.Error, message)};

    this.ready = function(callback) {
      document.addEventListener('readystatechange', function() {
          if(document.readyState == 'interactive') {
            callback();
          }
      });
    };

    this.findId = function(id) {
      return new utils.Collection(document.getElementById(id));
    };

    this.findClass = function(className) {
      return new utils.Collection(document.getElementsByClassName(className));
    };

    this.findTag = function(tagName) {
      return new utils.Collection(document.getElementsByTagName(tagName));
    }

    this.merge = function(obj1, obj2) {
      var obj = obj2;
      for(var prop in obj1) {
        obj[prop] = obj1[prop];
      }

      return obj;
    };

    this._data = function(object, data, defReturn) {
      if(typeof data == 'object') {
        if(typeof object.$utilsData == 'undefined') {
          object.$utilsData = data;
        }
        else {
          object.$utilsData = utils.merge(data, object.$utilsData);
        }
      }
      else if(typeof data == 'string') {
        if(typeof object.$utilsData == 'undefined'
        || typeof object.$utilsData[data] == 'undefined') {
          return defReturn;
        }
        else {
          return object.$utilsData[data];
        }
      }
    }

    this.vendorPrefixes = ['webkit','moz','ms','o'];

    this.prefixVendor = function(property, thisArg, type) {
      if(typeof thisArg[property] != 'function') {
          var property = this.vendorPrefixes.reduce(
            function(found, prefix) {
              if(found) {
                return found;
              }
              var prefixedProp = '{0}{1}'.format(
                prefix, property.upperCaseFirst()
              );

              if(typeof type == 'undefined')
              {
                if(typeof thisArg[prefixedProp] != 'undefined') {
                  return prefixedProp;
                }
              }
              else {
                if(typeof thisArg[prefixedProp] == type) {
                  return prefixedProp;
                }
              }

              return null;

          }, null);
      }

      return property;
    }

    this.compatibilityProperty = function(property, thisArg) {
      property = this.prefixVendor(property, thisArg);
      return thisArg[property];
    }

    this.compatibilityCall = function(functionName, thisArg) {
      functionName = this.prefixVendor(functionName, thisArg, 'function');

      if(typeof thisArg[functionName] == 'function') {
        return thisArg[functionName]
          .apply(thisArg, Array.from(arguments).slice(2));
      }
    }

    this.map = function(v, fn, thisArg) {
      if(typeof v == 'object') {
        if (typeof v.map == 'function') {
          return v.map(fn, thisArg);
        }
        else {
          var w = {};
          for(var prop in v) {
            w[prop] = fn(prop, v[prop]);
          }
          return w;
        }
      }

      util.error('Mapped value is not object.');
    };

    this.constr = function(context, selector) {
      if(typeof selector == 'undefined') {
        if(typeof context == 'string') {
          selector = context;
          context = utils(document);
        }
      }

      if(typeof context == 'object' && (! (context instanceof utils.Collection))) {
        context = new utils.Collection(context);
      }

      if(typeof selector == 'undefined') {
        return context;
      }
      else {
        return context.find(selector);
      }
    };

    this.isIterable = function(object) {
      return typeof object == 'object'
        &&
        ( (typeof object[Symbol.iterator] != 'undefined')
        || typeof object.length != 'undefined'
        || typeof object.forEach == 'function');
    }

    this.fullscreenSupported = function() {
      var root = document.documentElement;
      var fullscreenFn
        =  this.prefixVendor('requestFullScreen', root, 'function')
        || this.prefixVendor('requestFullscreen', root, 'function');

      return typeof root[fullscreenFn] == 'function';
    }

    this.isFullscreen = function() {
      return null != // Stupid inconsistency.
        (  this.compatibilityProperty('fullScreenElement', document)
        || this.compatibilityProperty('fullscreenElement', document));
    }

    this.exitFullscreen = function() {
      var fn = this.prefixVendor('exitFullscreen', document, 'function')
        || this.prefixVendor('cancelFullScreen', document, 'function');

      if(fn) {
        document[fn].call(document);
      }
    }

    this.html = function(html) {
      if(html.indexOf('<') != -1) {
        var container = document.createElement('div');
        container.innerHTML = html;

        return utils(container.children);
      }
      else {
        return utils(document.createElement(html));
      }
    }
  };

  utils.init();
};

var $ = utils;

/***************************** Presentation **********************************/

var pres = function(config) {

  this.defConfig = {
    frameClass: 'snimek',
    frameRatio: 4/3,
    zoomLevel: 4,
    endSize: 0.2,
    baseWidth: 1280
  };

  this.activeFrame = null;
  this.isFullscreen = false;

  this.config = $.merge(config, this.defConfig);

  this.init = function() {
    var config = this.config;
    var container = $.html('div').addClass('pres-container');
    var endFrame = $.html('div')
      .prop({innerHTML:'<div>END</div>'})
      .addClass(['pres-frame', 'pres-end-frame']);

    this.container = container.firstElement();
    var userFrames = $.findClass(config.frameClass);
    var middleFrames = userFrames
      .elements
      .map(function(frame, index) {
        var middleFrame = $.html('div')
          .appendChild(frame);
        var presFrame = $.html('div')
          .addClass('pres-frame')
          .appendChild(middleFrame)
          .prop({
            id: 'pres-frame-id-{0}'.format(index)
          });

        container.appendChild(presFrame);
        return middleFrame;
      });

    this.frames = $(middleFrames);
    this.outerFrames = container.findClass('pres-frame');
    this.outerFrames.add(endFrame);
    this.controls = this.createControls();
    this.endFrame = endFrame;

    $.findTag('body')
      .empty()
      .appendChild([container, this.controls]);

    this.frames.css({
      width: '{0}px'.format(this.config.baseWidth),
      height: '{0}px'.format(this.config.baseWidth / this.config.frameRatio)
    });

    this.attachListeners();

    this.update();
  }

  this.attachListeners = function() {
    var that = this;
    var controls = this.controls.ctrl;

    window.addEventListener('resize', function() {
      that.update();
    });

    controls.zoom.on(['input','change'], function() {
      that.setZoom(this.value);
    });

    var navControl = [
      controls.first,
      controls.prev,
      controls.play,
      controls.stop,
      controls.next,
      controls.last
    ];

    $(navControl)
      .on('click', function(event) {

        var action = 'nav{0}'.format(this.navAction.upperCaseFirst());

        if(typeof that[action] == 'function') {
          that[action]();
        }
        else {
          $.warn('Undefined action.');
        }

        event.preventDefault();
      });

    this.outerFrames
      .on('click', function(event) {
        if(that.isFullscreen) {
          return;
        }
        else if(this == that.activeFrame) {
          that.selectFrame(null, true);
        }
        else {
          that.selectFrame(this);
        }

        event.preventDefault();
      })
      .on('dblclick', function(event) {
        if(that.isFullscreen) {
          return;
        }

        that.selectFrame(this);
        that.fullscreen();
        event.preventDefault();
      })
      .on('dragstart', function(event) {
        // Cannot use setData of dataTransfer because getData returns nothing
        // in IE in dragover event handler, but call to setData is necessary
        // for Firefox, otherwise it doesn't work.

        var source = event.target;
        if(!$(source).hasClass('pres-frame')) {
          source = $(event.target)
            .parents()
            .elements
            .find(function(parent) {
              return $(parent).hasClass('pres-frame');
            });
        }

        if(!source) {
          event.dataTransfer.effectAllowed = 'none';
          return;
        }

        event.dataTransfer.setData('text', '');
        event.dataTransfer.effectAllowed = 'move';
        if(typeof event.dataTransfer.setDragImage == 'function') {
          var rect = source.getBoundingClientRect();
          var x = event.clientX - rect.left;
          var y = event.clientY - rect.top;
          event.dataTransfer.setDragImage(source,x,y);
        }
        that.presDragSource = source;

        var target = $(source)
          .addClass('pres-drag-source');

        that.selectFrame(null, true);
      }, true)
      .on(['dragend','drop'], function(event) {
        var target = $(event.target);
        that.presDragSource = null;

        target.removeClass('pres-drag-source');
        event.preventDefault();
      });

    $(this.container)
      .on(['dragenter','dragover'], function(event) {
        var source = that.presDragSource;
        var dest = $(event.target)
          .parents()
          .elements
          .find(function(parent) {
            return $(parent).hasClass('pres-frame');
          });

        if(!dest || !source) {
          return;
        }

        event.preventDefault();
        event.dataTransfer.dropEffect = 'move';

        if(source != dest) {
          var rect =  dest.getBoundingClientRect();
          var left = event.clientX - rect.left;
          var right = rect.right - event.clientX;
          var insertBefore = left < right;

          if(source.nextElementSibling == dest) {
            insertBefore = false;
          }
          else if(source.previousElementSibling == dest) {
            insertBefore = true;
          }

          if(insertBefore) {
            $(source).insertBefore(dest);
          }
          else {
            $(source).insertAfter(dest);
          }
        }
      })

    $(document)
      .on('keydown', function(event) {
        // 'keydown' has to be used because 'keypress' event doesn't detect
        // arrow keys.
        switch(event.code || event.key) {
          case 'Enter':
          case 'Space':
            if(that.isFullscreen) that.navNext();
            else that.navPlay();

            event.preventDefault();
            break;

          case 'PageUp':
            if(!that.isFullscreen) break;
          case 'ArrowRight':
          case 'Right':
            that.navNext();
            event.preventDefault();
            break;

          case 'PageDown':
            if(!that.isFullscreen) break;
          case 'ArrowLeft':
          case 'Left':
            that.navPrev();
            event.preventDefault();
            break;

          case 'Home':
            that.navFirst();
            event.preventDefault();
            break;

          case 'End':
            that.navLast();
            event.preventDefault();
            break;

          case 'Escape':
          case 'Esc':
            if(that.isFullscreen) that.navStop();
            else that.selectFrame(null, true);
            break;
        }
      })
      .on('mousemove', function(event) {
        that.updateControls({
          x: event.clientX,
          y: event.clientY
        });
      });

    var fullscreenchange = $.vendorPrefixes.map(''.format, '{0}fullscreenchange');
    fullscreenchange.push('fullscreenchange');
    $([document,document.documentElement]).on(fullscreenchange,
      function(event){
        if(!$.isFullscreen()) {
          $.info('Fullscreen exit: Entering preview.');
          that.preview();
        }
        else {
          $.info('Fullscreen enter.');
          that.fullscreen();
        }
      }
    );
  }

  this.updateControls = function(mousePosition) {
    var ctrlElement = this.controls.firstElement();
    var ctrlWidth = ctrlElement.offsetWidth;
    var ctrlHeight = ctrlElement.offsetHeight;

    if(this.isFullscreen) {
      if(mousePosition) {
        var screenHeight = document.documentElement.clientHeight;
        var ctrlX = ctrlWidth/2;
        var ctrlY = screenHeight - ctrlHeight/2;

        var distX = Math.max(Math.abs(ctrlX - mousePosition.x) - ctrlWidth / 2, 0);
        var distY = Math.max(Math.abs(ctrlY - mousePosition.y) - ctrlHeight / 2, 0);

        this.controls.css({
          left: '{0}px'.format(Math.max(-distX * 5, -ctrlWidth)),
          bottom: '{0}px'.format(Math.max(-distY * 2, -ctrlHeight))
        });
      }
      else {
        this.controls.css({
          left: '{0}px'.format(-ctrlWidth),
          bottom: '{0}px'.format(-ctrlHeight)
        });
      }
    }
    else {
      this.controls.css({
        bottom: '',
        left: ''
      })
    }
  }

  this.setZoom = function(zoomLevel) {
    this.config.zoomLevel = zoomLevel;
    this.preview();
  }

  this.computeFrameDimensions = function(scale) {
    var frame = this.outerFrames.firstElement();
    var root = document.documentElement;
    var screenWidth = root.clientWidth;

    var style = window.getComputedStyle(frame);
    var borderH = parseFloat(style.borderLeftWidth) + parseFloat(style.borderRightWidth);
    var marginH = parseFloat(style.marginLeft) + parseFloat(style.marginRight);
    var paddingH = parseFloat(style.paddingLeft) + parseFloat(style.paddingRight);
    var borderV = parseFloat(style.borderTopWidth) + parseFloat(style.borderBottomWidth);
    var paddingV = parseFloat(style.paddingTop) + parseFloat(style.paddingBottom);
    // var marginV = parseFloat(style.marginTop) + parseFloat(style.marginBottom);

    var frameSpace = screenWidth * scale; // Space for single frame.
    var frameWidth = frameSpace - marginH;
    var frameHeight = frameWidth / this.config.frameRatio;

    // Width of screen without margins and borders of frames.
    var spaceFreeWidth = screenWidth - ((marginH + borderH) / scale);

    return {
      width: frameWidth - (paddingH + borderH),
      height: frameHeight - (paddingV + borderV),
      scale: scale * (spaceFreeWidth / this.config.baseWidth)
    };
  }

  this.computeFullscreenDimensions = function() {
    var root = document.documentElement;
    var screenWidth = root.clientWidth;
    var screenHeight = root.clientHeight;

    var dims = {};

    var ratioHeight = root.clientWidth / this.config.frameRatio;

    if(ratioHeight > screenHeight) {
      dims.width = screenHeight * this.config.frameRatio;
      dims.height = screenHeight;
    }
    else {
      dims.width = screenWidth;
      dims.height = ratioHeight;
    }

    dims.scale = dims.width / this.config.baseWidth;

    return dims;
  }

  this.update = function() {
    if(this.isFullscreen) {
      this.fullscreen();
    }
    else {
      this.preview();
    }
  }

  this.selectFrame = function(frame, allowNull) {
    if(frame || allowNull) {
      this.outerFrames.removeClass('pres-active');
      this.activeFrame = frame;
    }

    if(frame) {
      $(frame).addClass('pres-active');
    }

    this.update();
  };

  this.hasActiveFrame = function() {
    return this.activeFrame != null
      && !this.isEndFrame();
  }

  this.isEndFrame = function() {
    return this.activeFrame == this.endFrame.firstElement();
  }

  this.isFirstFrame = function() {
    return this.activeFrame == this.container.firstElementChild;
  }

  this.navFirst = function() {
    $.info('Navigation first');
    this.selectFrame(this.container.firstElementChild);
  }

  this.navPrev = function() {
    $.info('Navigation prev');
    if(this.activeFrame) {
      this.selectFrame(this.activeFrame.previousElementSibling);
    }
    else {
      this.selectFrame(this.container.lastElementChild);
    }
  }

  this.navNext = function() {
    $.info('Navigation next');
    if(this.hasActiveFrame()) {
      this.selectFrame(this.activeFrame.nextElementSibling);
    }
    else if(!this.isEndFrame()) {
      this.selectFrame(this.container.firstElementChild);
    }
  }

  this.navLast = function() {
    $.info('Navigation last');
    this.selectFrame(this.container.lastElementChild);
  }

  this.navPlay = function() {
    $.info('Navigation play');
    if(!this.hasActiveFrame()) {
      this.navFirst();
    }
    this.fullscreen();
    this.update();
  }

  this.navStop = function() {
    $.info('Navigation stop');
    this.preview();
    this.update();
  }

  this.preview = function() {
    var root = $(document.documentElement);

    if(this.isFullscreen) {
      this.isFullscreen = false;
      root.removeClass('pres-on-fullscreen');
      this.endFrame.remove();
      this.updateControls();
      if($.fullscreenSupported()) {
        $.exitFullscreen();
        window.setTimeout(this.preview.bind(this), 100); // Wait for change.
        return;
      }
    }

    var scale = 1/this.config.zoomLevel;
    var dims = this.computeFrameDimensions(scale);

    this.frames.css({
      transform: 'scale( {0} )'.format(dims.scale),
      transformOrigin: '0 0'
    });

    this.outerFrames
      .show()
      .css({
        width: '{0}px'.format(dims.width),
        height: '{0}px'.format(dims.height)
      })
      .prop({
        draggable: true
      });

    var ctrl = this.controls.ctrl;
    ctrl.play.show();
    ctrl.zoomContainer.show();
    ctrl.stop.hide();
  }

  this.fullscreen = function() {
    var root = $(document.documentElement);

    if(!this.isFullscreen) {
      this.isFullscreen = true;
      root.addClass('pres-on-fullscreen');
      $(this.container).appendChild(this.endFrame);
      this.updateControls();
      if($.fullscreenSupported()) {
        root.requestFullscreen();
        window.setTimeout(this.fullscreen.bind(this), 100); // Wait for change.
        return;
      }
    }

    var dims = this.computeFullscreenDimensions();

    this.frames
      .css({
        transform: 'scale( {0} )'.format(dims.scale),
        transformOrigin: '0 0'
      });

    this.outerFrames
      .hide()
      .css({
        width: '{0}px'.format(dims.width),
        height: '{0}px'.format(dims.height)
      })
      .prop({
        draggable: false
      });;

    this.endFrame.css({
      lineHeight: '{0}px'.format(dims.height),
      fontSize: '{0}px'.format(dims.height * this.config.endSize)
    });

    $(this.activeFrame).show();

    var ctrl = this.controls.ctrl;
    ctrl.play.hide();
    ctrl.zoomContainer.hide();
    ctrl.stop.show();
  }

  this.createControls = function() {
    var controls = $.html('div');
    var navContainer = $.html('div');
    var zoomContainer = $.html('div');
    var zoom = $.html('input');

    controls.addClass('pres-controls');
    navContainer.addClass('pres-navigation');
    zoomContainer.addClass('pres-zoom');

    zoom
      .prop({
        type: 'range',
        min: 3,
        max: 6,
        defaultValue: this.config.zoomLevel
      });

    var nav = {
      first: {label: '&#x23ee;', title:'First frame'},
      prev: {label: '&#x23ea;', title: 'Previous frame'},
      play: {label: '&#x25b6;', title: 'Enter presentation mode'},
      stop: {label: '&#x2b1b;', title: 'Exit presentation mode'},
      next: {label: '&#x23e9;', title: 'Next frame'},
      last: {label: '&#x23ed;', title: 'Last frame'}
    };

    var navElems = $.map(nav,function(key, value) {
      var navElem = $.html('button')
        .addClass(['pres-nav-elem', 'pres-nav-{0}'.format(key)])
        .prop({
          title: value.title,
          innerHTML: value.label,
          navAction: key
        });

      navContainer.appendChild(navElem);

      return navElem;
    });

    var ctrl = $.merge(navElems,{
      zoom: zoom, // &#x1f50d; magnifying glass
      navigationContainer: navContainer,
      zoomContainer: zoomContainer
    });

    controls.ctrl = ctrl;

    navContainer.appendChild(navElems);
    zoomContainer.appendChild(zoom);
    controls.appendChild([navContainer,zoomContainer]);

    return controls;
  }

  this.init();
};

pres.create = function() {
  var config = pres.config || {};

  if(!pres.instance) {
    pres.instance = new pres(config);
  }
};

pres.setup = function(config) {
  pres.config = config;
}

/***************************** Other stuff ***********************************/
$.ready(function(){
  pres.create();
});
